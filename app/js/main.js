import React from 'react';
import { render } from 'react-dom';
import App from './components/App';
import '../../node_modules/bootstrap/dist/css/bootstrap.css';
import '../sass/styles.scss';

render(
  <App />,
  document.getElementById('root'),
);
