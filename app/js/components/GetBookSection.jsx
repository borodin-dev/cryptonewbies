import React, { Component } from 'react';
import { Parallax } from 'react-parallax';

export default class GetBookSection extends Component {
  render() {
    const parallaxImage = "https://d9hhrg4mnvzow.cloudfront.net/ebook.cryptonewbies.com/6257acde-gettyimages-675913278-cropped.jpg";
    return (
      <React.Fragment>
        <section className="get-book-section">
          <div className="get-book-section__color-overlay">
            <div className="container align-center">
              <div className="get-book-section__title">
                <h3>Cryptocurrency is a booming opportunity right now!</h3>
                <h2>Don&apos;t get left behind.</h2>
              </div>
              <div className="row get-book-section__list">
                <div className="col-lg-7 offset-lg-1">
                  <p className="ebook-list__title">
                    <strong>It&apos;s easier than you think!
                      In this one-of-a-kind ebook you&apos;ll learn:</strong>
                  </p>
                  <ul className="ebook-list">
                    <li><span>What cryptocurrency is (minus the headache)</span></li>
                    <li><span>How to buy cryptocurrency easily with dollars, euro, etc.</span></li>
                    <li><span>How to trade and pick up &apos;altcoins&apos;</span></li>
                    <li><span>WTF a wallet and keys are</span></li>
                    <li><span>How to keep cryptocurrency safe from hackers</span></li>
                    <li><span>Common investment strategies</span></li>
                  </ul>
                </div>
                <div className="col-lg-4">
                  <a className="btn btn-book">Get the Book!</a>
                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}
