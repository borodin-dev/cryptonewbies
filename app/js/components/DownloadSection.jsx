import React, { Component } from 'react';

export default class DownloadSection extends Component {

  render() {
    return (
      <React.Fragment>
        <section className="download-section">
          <div className="container">
            <div className="row">
              <div className="col-lg-8  download-section__content">
                <h1 className="download-section__text">Download a free chapter</h1>
              </div>
              <div className="col-lg-3  download-section__btn">
                <a href="#" className="download-section__btn-dwnld">Yes, please.</a>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}
