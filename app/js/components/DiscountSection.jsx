import React, { Component } from 'react';

export default class DiscountSection extends Component {

  render() {
    return (
      <React.Fragment>
        <section className="discount-section">
          <div className="discount-section-overlay-color">
            <div className="container">
              <div className="row">
                <div className="col-lg-9 offset-lg-2 discount-section__content">
                  <h1 className="discount-section__text">Limited Time Discount</h1>
                  <p className="discount-section__subtext">Receive 50% off through July 14, 2018!</p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}
