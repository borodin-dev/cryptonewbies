import React, { Component } from 'react';

export default class FooterSection extends Component {

  render() {
    return (
      <React.Fragment>
        <section className="footer-section">
          <div className="container">
            <div className="row">
              <div className="col-lg-11 offset-lg-1  footer-section__text">
                <p>Copyright © 2017 Ryan Oelke, Cryptocurrency for Newbies</p>
                <a href="#">Terms and Disclaimer</a>
                <a href="#">Affiliate Disclosure</a>
                <a href="#">Privacy</a>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}
