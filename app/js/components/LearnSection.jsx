import React, {Component} from 'react';

export default class LearnSection extends Component {

  render() {
    return (
      <React.Fragment>
        <section className="learn-section">
          <div className="container">
            <div className="learn-section__title">
              <div className="row">
                <div className="col-lg-10 offset-lg-1">
                  <h3>Learn to navigate the world of Bitcoin,<br/>
                    Ethereum, and altcoins, whether you’re investing<br/>
                    $50 or thousands.</h3>
                  <div className="learn-section__subtitle">
                    <p>I&apos;ve structured this ebook in exactly the way that
                      I needed when I first started exploring cryptocurrency:
                      organized, easy-to-follow, down-to-earth. </p>
                    <p>I address your most burning questions, common questions
                      I see online everyday from friends and cryptocurrency
                      newbies everywhere on Facebook. </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="learn-section__content">
              <div className="row">
                <div className="col-lg-6 offset-lg-2">
                  <h4>Table of Contents Sneak Peek</h4>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-7 offset-lg-1 learn-section__content-part1">
                  <p>Part 1: What is cryptocurrency?</p>
                  <ul>
                    <li><span>Time to tickle your FOMO</span></li>
                    <li><span>Understanding cryptocurrency, one bit at a time </span></li>
                    <li><span>How are cryptocurrencies being used?</span></li>
                    <li><span>How are cryptocurrencies being used? </span></li>
                  </ul>
                </div>
                <div className="col-lg-4 img-center">
                  <img src="./assets/images/learn-im1.jpg" alt="cryptocurrency"/>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-7 offset-lg-1 learn-section__content-part2">
                  <p>Part 2: Why is cryptocurrency important?</p>
                  <ul>
                    <li><span>WTF is Blockchain </span></li>
                    <li><span>But first, WTF is a Ledger.
                      Let’s start with a financial ledger.</span></li>
                    <li><span>Standout Benefits of Cryptocurrency </span></li>
                  </ul>
                </div>
                <div className="col-lg-4 img-center">
                  <img src="./assets/images/learn-im2.jpg" alt="dollar"/>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-7 offset-lg-1 learn-section__content-part3">
                  <p>Part 3: How to buy, trade, and sell cryptocurrency</p>
                  <ul>
                    <li><span>Important nuts and bolts before buying cryptocurrencies </span></li>
                    <li><span>Buying cryptocurrencies with traditional money (dollar, euro, etc) </span></li>
                    <li><span>An example: Buying cryptocurrency on Coinbase </span></li>
                    <li><span>How to transfer from one wallet to another wallet </span></li>
                    <li><span>Trading cryptocurrency on exchanges </span></li>
                    <li><span>How to Trade Cryptocurrency - Bittrex as an Example </span></li>
                    <li><span>Illustrative Instructions for a Simple Trade on Bittrex</span></li>
                    <li><span>How to Sell Cryptocurrency</span></li>
                  </ul>
                </div>
                <div className="col-lg-4 img-center">
                  <img src="./assets/images/learn-im3.jpg" alt="lock"/>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-7 offset-lg-1 learn-section__content-part4">
                  <p>Part 4: Keeping your cryptocurrency safe</p>
                  <ul>
                    <li><span>Wallet Security Strong Passwords </span></li>
                    <li><span>Phishing - Don’t take the bait.</span></li>
                    <li><span>Two-step authentication (2FA)</span></li>
                    <li><span>Keeping your phone and phone number secure</span></li>
                    <li><span>Secure your internet connection </span></li>
                    <li><span>Keeping physical backups safe</span></li>
                    <li><span>Subscribe to provider newsletters, social media accounts,
                      and status websites Nothing is foolproof. </span></li>
                    <li><span>Look at all of your options.</span></li>
                  </ul>
                </div>
                <div className="col-lg-4 img-center">
                  <img src="./assets/images/learn-im4.jpg" alt="dollar graph"/>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-8 offset-lg-1 learn-section__content-part5">
                  <p>Part 5: Tracking your portfolio and investment strategies</p>
                  <ul>
                    <li><span>Tracking your cryptocurrency portfolio </span></li>
                    <li><span>Cryptocurrency Investment and Common Trading Strategies</span></li>
                  </ul>
                </div>
                <div className="col-lg-1"></div>
              </div>
              <div className="row">
                <div className="col-lg-7 offset-lg-1 learn-section__content-part6">
                  <p>Bonus content</p>
                  <ul>
                    <li><span>The Basics of Reading Price Charts </span></li>
                    <li><span>What’s the ‘crypto’ in cryptocurrency mean? </span></li>
                    <li><span>Special Topics</span>
                      <ul>
                        <li><span>Mining</span></li>
                        <li><span>Scaling</span></li>
                        <li><span>Forking</span></li>
                        <li><span>Governance</span></li>
                        <li><span>ICOs (Initial Coin Offering) </span></li>
                      </ul>
                    </li>
                    <li><span>Quick Cryptocurrency Resource List</span></li>
                    <li><span>Bite-Size Cryptocurrency Glossary</span></li>
                  </ul>
                </div>
                <div className="col-lg-4 img-center">
                  <img src="./assets/images/learn-im5.jpg" alt="wow"/>
                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}
