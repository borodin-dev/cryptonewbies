import React, { Component } from 'react';

export default class AuthorSection extends Component {

  render() {
    return (
      <React.Fragment>
        <section className="author-section">
          <div className="container">
            <div className="row">
              <div className="col-lg-5 offset-lg-4">
                <h1 className="author-section__text">About the Author Ryan Oelke</h1>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-5 offset-lg-2 author-section__content">
                <p>
                  Ryan Oelke, BA and MSEd, is the founder and CEO of the digital
                  creative agency, <a className="author-section__content-link1" href="https://www.powerupproductions.tv/" target="_blank">PowerUp Productions</a>,
                  and a <a className="author-section__content-link2" href="https://cryptoconsortium.org/lookup/0e41eb" target="_blank"><strong>Certified Bitcoin Professional</strong></a>.
                  He has been a teacher for over 17 years, in-person and online, in the business world and in higher education.
                </p>
                <p>
                  Ryan has taught and mentored a variety of subjects and fields including business, marketing, productivity, WordPress, branding, languages, physics, philosophy, meditation, and of course, cryptocurrency.
                </p>
                <p>
                  Ryan lives and works in Asheville, NC.
                </p>
              </div>
              <div className="col-lg-3 author-section__content-photo">
                <img src="./assets/images/author-photo.jpg" alt="author photo"/>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-12 author-section__content-logo">
                <img src="./assets/images/cryptocurrency-logo.png" alt="cryptocurrency logo"/>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}
