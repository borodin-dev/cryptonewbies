import React, { Component } from 'react';

export default class GetCopyFooterSection extends Component {

  render() {
    return (
      <React.Fragment>
        <section className="get-copy-footer-section">
          <div className="container">
            <div className="row">
              <div className="col-lg-12  get-copy-footer-section__content">
                <h1 className="get-copy-footer-section__text">The cryptocurrency revolution awaits. </h1>
                <p className="get-copy-footer-section__subtext">Get the full 179-page, easy-to-follow ebook for only $29.99.</p>
                <a className="get-copy-footer-section__btn" href="#">GET YOUR COPY TODAY</a>
                <p className="get-copy-footer-section__subtext">Immediate download after purchase.</p>
                <h2 className="get-copy-footer-section__bottom-txt"><strong>Get answers. Get clarity. Get going.</strong></h2>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}
