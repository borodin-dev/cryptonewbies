import React, { Component } from 'react';

export default class GetCopySection extends Component {

  render() {
    return (
      <React.Fragment>
        <section className="get-copy-section">
          <div className="container">
            <div className="row">
              <div className="col-lg-5 offset-lg-1 get-copy-section__img">
                <img src="./assets/images/get-copy-crypto-book.png" alt="crypto book" />
              </div>
              <div className="col-lg-3 offset-lg-1 get-copy-section__content">
                <a href="#" className="btn-get-copy">Get Your Copy Today!</a>
                <p>Only <s>$49.99</s> $29.99 between now and July 4, 2018.</p>
                <p>*Immediate digital download!</p>
                <p>179 pages of crypto clarity.</p>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}
