import React, { Component } from 'react';


export default class HeadSection extends Component {

  render() {
    return (
      <React.Fragment>
        <section className="background-clr">
          <div className="container">
            <div className="section-content col-lg-12">
              <div className="section-content__title">
                <h1 className="section-content__fw"> { /* first word */ }
                  <span>CRYPTOCURRENCY</span>
                </h1>
                <h1 className="section-content__sw"> { /* second word */ }
                  <span>FOR NEWBIES</span>
                </h1>
              </div>
              <div className="section-content__subtitle">
                <h2 className="section-content__sbt-txt"> { /* subtitle text */ }
                  <span>A Beginner&apos;s Guide to Understanding,
                    Buying,
                    and Selling
                    Cryptocurrency&nbsp;
                  </span>
                </h2>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}
