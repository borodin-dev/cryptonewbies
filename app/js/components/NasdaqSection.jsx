import React, {Component} from 'react';

export default class NasdaqSection extends Component {

  render() {
    return (
      <React.Fragment>
        <section className="nasdaq-section">
          <div className="nasdaq-section-overlay-color">
            <div className="container">
              <div className="row">
                <div className="col-lg-9 offset-lg-2 nasdaq-section__content">
                  <h2 className="nasdaq-section__text">“The Nasdaq got to $5.4 trillion in 1999, why couldn&apos;t cryptocurrency be as big?
                    There&apos;s so much human capital and real money being poured into the space
                    and we&apos;re at the takeoff point.” </h2>
                  <p className="nasdaq-section__author"><strong>Michael Novogratz</strong>, Billioniare Ex-Hedge Fund Manager with 10% of his holdings in cryptocurrency</p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}
