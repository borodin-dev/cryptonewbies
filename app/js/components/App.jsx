import React from 'react';


import HeadSection from './HeadSection';
import GetBookSection from './GetBookSection';
import NasdaqSection from './NasdaqSection';
import NonGeeksSection from "./NonGeeksSection";
import LearnSection from "./LearnSection";
import DiscountSection from "./DiscountSection";
import GetCopySection from "./GetCopySection";
import DownloadSection from "./DownloadSection";
import AuthorSection from "./AuthorSection";
import GetCopyFooterSection from "./GetCopyFooterSection";
import FooterSection from "./FooterSection";

export default function App() {
  return (
    <React.Fragment>
      <HeadSection />
      <GetBookSection />
      <NasdaqSection />
      <NonGeeksSection />
      <LearnSection />
      <DiscountSection />
      <GetCopySection />
      <DownloadSection />
      <AuthorSection />
      <GetCopyFooterSection />
      <FooterSection />
    </React.Fragment>
  );
}
