import React, {Component} from 'react';

export default class NonGeeksSection extends Component {

  render() {
    return (
      <React.Fragment>
        <section className="non-geeks-section">
          <div className="container">
            <div className="row">
              <div className="col-lg-8 offset-lg-2">
                <h2 className="non-geeks-section__title">Written in plain-speak for non-geeks</h2>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-2 offset-lg-1 non-geeks-section__img">
                <img src="./assets/images/non-geeks-rocket.jpg" alt="rocket" />
              </div>
              <div className="col-lg-7 non-geeks-section__content">
                <p>Most cryptocurrency guides make you feel like
                  you need a degree in computer science just to get started.</p>
                <p>Cryptocurrency can be explained so much more simply.</p>
                <p>I&apos;ve written this ebook for everyday, non-techy folks,
                  whether you&apos;ve already taken your first steps, you&apos;re
                  considering buying cryptocurrency, or you simply want to know
                  what the hell is going on and why more and more people are
                  raving about cryptocurrency.</p>
                <p><strong>Check out the table of contents below!</strong></p>
                <p className="non-geeks-section__content-arrow">↓</p>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}
